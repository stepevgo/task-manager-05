# TASK MANAGER

## DEVELOPER INFO

**NAME**: Evgeniy Stepanishchev

**EMAIL**: estepanischev@t1-consulting.ru

**WORK EMAIL**: stepevgo.vrn@gmail.com

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: WINDOWS 10 PRO 21H1

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM
```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```


